import 'package:flutter/material.dart';

void main() {
  runApp(const MyFirsApp());
}

class MyFirsApp extends StatelessWidget {
  const MyFirsApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.insert_emoticon),
          title: Text('My First App'),
          actions: [
            IconButton(
                onPressed: (){},
                icon:Icon(Icons.add_alarm),
    )],
        ),
        body: Center(
          child: Column(
            children: [
              Image.asset('assets/images/kim.jpg',
              height: 300,
              width: 250,),
              Text('นางสาว ปรมาภรณ์ ชูหนู',style: TextStyle(height: 2, fontSize: 30) ),
              Text('6350110024 ' ,style: TextStyle(height: 2,fontSize: 20),),
          ],
        ),
      ),
    ),);
  }
}
